#!/bin/sh

starting() {
    echo "$TIMESTAMP >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "
}

ending() {
    echo "$TIMESTAMP <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< "
    echo
}

export GCCARMBIN=/app/mcuxpresso-ide/usr/local/mcuxpressoide-10.1.0_589/ide/tools/bin/

TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")

cd res-nxh3770 || exit 1

starting "KL apps"
make -C sw/kinetis/KL27/ debug
ending "KL apps"

starting "KL deps"
make -C sw/kinetis/KL27/ deps
ending "KL deps"

